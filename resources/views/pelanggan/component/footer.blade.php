<div class="d-flex justify-content-between gap-5 pt-4 mt-5">
    <div class="title-left w-25">
        <div class="header-title fs-4 mb-2 font-weight-bolder">
            Baby Needs
        </div>
        <p>Baby Needs adalah aplikasi yang didedikasikan untuk menyediakan segala kebutuhan
            bayi Anda dengan produk berkualitas tinggi. Kami memahami betapa pentingnya kenyamanan
            dan keselamatan bayi Anda, oleh karena itu kami hanya menawarkan produk-produk terbaik yang telah teruji.
        </p>
    </div>
    <div class="title-middle w-25">
        <div class="header-title fs-4 mb-2 font-weight-bolder">
            Tentang Kami
        </div>
        <ul class="list-unstyled">
            <li>
                <p>
                    Baby Needs hadir untuk mempermudah Anda dalam memenuhi kebutuhan si kecil.
                    Dengan berbagai produk pilihan yang aman dan nyaman, kami siap mendampingi Anda
                    dalam merawat buah hati tercinta.
                </p>
            </li>
            <li>
                <div class="d-flex gap-3">
                    <div>
                        <i class="fa-solid fa-phone"></i>
                    </div>
                    <div>
                        <p>(+62)812 3456 7890</p>
                    </div>
                </div>
            </li>
            <li>
                <div class="d-flex gap-3">
                    <div>
                        <i class="fa-solid fa-envelope"></i>
                    </div>
                    <div>
                        <p>BabyNeeds@gmail.com</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="title-sosmed">
        <div class="header-title fs-5 mb-2 font-weight-bold">
            Sosial Media
        </div>
        <div class="sosmed d-flex flex-column gap-2">
            <div class="d-flex align-items-center gap-2">
                <i class="fab fa-instagram fa-lg"></i>
                <span class="fs-6">Baby Needs</span>
            </div>
            <div class="d-flex align-items-center gap-2">
                <i class="fab fa-facebook fa-lg"></i>
                <span class="fs-6">Baby Needs</span>
            </div>
            <div class="d-flex align-items-center gap-2">
                <i class="fab fa-whatsapp fa-lg"></i>
                <span class="fs-6">089123456789</span>
            </div>
            <div class="d-flex align-items-center gap-2">
                <i class="fab fa-tiktok fa-lg"></i>
                <span class="fs-6">Baby Needs</span>
            </div>
        </div>
    </div>

</div>
<div class="text-center p-lg-4 mt-3">
    Copy right @BabyNeeds 2024</i>
</div>
