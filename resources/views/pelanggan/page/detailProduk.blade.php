@extends('pelanggan.layout.index')

@section('content')
    <div class="container mt-5 pt-5">
        <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);"
            aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Detail</li>
            </ol>
        </nav>
    </div>

    <div class="container mt-3 bg-light">
        <div class="row">
            <div class="col-md-6 my-3">
                <img src="{{ asset('storage/product/' . $product->foto) }}" alt="{{ $product->nama_product }}"
                    style="width: 100%; height: auto;">
            </div>
            <div class="col-md-6 my-3">
                <h1>{{ $product->nama_product }}</h1><br>
                <p style="font-size: 24px; font-weight: bold;">IDR {{ number_format($product->harga) }}</p>
                <form action="{{ route('addTocart') }}" method="POST">
                    @csrf
                    <input type="hidden" name="idProduct" value="{{ $product->id }}">
                    <button type="submit" class="btn btn-outline-primary" style="font-size:24px">
                        <i class="fa-solid fa-cart-plus"></i> Add to Cart
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
