@extends('pelanggan.layout.index')

@section('content')
    <div class="d-flex gap-2 mt-5 pt-5">
        <div class="" style="width: 30%;">
            <div class="card" style="width: 18rem;">
                <div class="card-header">
                    Kategori
                </div>
                <div class="card-body">
                    <div class="accordion accordion-flush" id="accordionFlushExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#flush-collapseOne" aria-expanded="false"
                                    aria-controls="flush-collapseOne">
                                    <i class="fa-solid fa-shirt me-2"></i>
                                    Pakaian Bayi
                                </button>
                            </h2>
                            <div id="flush-collapseOne" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <div class="d-flex flex-column gap-4">
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory" value="Baju Tidur">
                                            <span>Baju Tidur</span>
                                        </div>
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory" value="Bodysuit">
                                            <span>Bodysuit</span>
                                        </div>
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory"
                                                value="Topi & Kaus Kaki">
                                            <span>Topi & Kaus Kaki</span>
                                        </div>
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory" value="Jaket & Selimut">
                                            <span>Jaket & Selimut</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#flush-collapseTwo" aria-expanded="false"
                                    aria-controls="flush-collapseTwo">
                                    <i class="fa-solid fa-bath me-2"></i>
                                    Perlengkapan Mandi
                                </button>
                            </h2>
                            <div id="flush-collapseTwo" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <div class="d-flex flex-column gap-4">
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory" value="Sabun">
                                            <span>Sabun</span>
                                        </div>
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory" value="Shampoo">
                                            <span>Shampoo</span>
                                        </div>
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory" value="Handuk">
                                            <span>Handuk</span>
                                        </div>
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory" value="Bak Mandi">
                                            <span>Bak Mandi</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#flush-collapseThree" aria-expanded="false"
                                    aria-controls="flush-collapseThree">
                                    <i class="fa-solid fa-utensils me-2"></i>
                                    Peralatan Makan & Minum
                                </button>
                            </h2>
                            <div id="flush-collapseThree" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <div class="d-flex flex-column gap-4">
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory" value="Botol Susu">
                                            <span>Botol Susu</span>
                                        </div>
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory" value="Dot">
                                            <span>Dot</span>
                                        </div>
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory"
                                                value="Sendok & Garpu">
                                            <span>Sendok & Garpu</span>
                                        </div>
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory"
                                                value="Piring & Mangkuk">
                                            <span>Piring & Mangkuk</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingFour">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#flush-collapseFour" aria-expanded="false"
                                    aria-controls="flush-collapseFour">
                                    <i class="fa-solid fa-suitcase-medical me-2"></i>
                                    Peralatan Kesehatan
                                </button>
                            </h2>
                            <div id="flush-collapseFour" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <div class="d-flex flex-column gap-4">
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory" value="Termometer">
                                            <span>Termometer</span>
                                        </div>
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory"
                                                value="Krim Ruam Popok">
                                            <span>Krim Ruam Popok</span>
                                        </div>
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory"
                                                value="Aspirator Hidung">
                                            <span>Aspirator Hidung</span>
                                        </div>
                                        <div class="d-flex flex-row gap-3">
                                            <input type="checkbox" name="kategory" class="kategory" value="Obat-obatan">
                                            <span>Obat-obatan</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Continue for other categories... -->
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-wrap gap-4 mb-5" id="filterResult" style="flex: 1;">
            @if ($data->isEmpty())
                <h1>Belum ada produk ...!</h1>
            @else
                @foreach ($data as $p)
                    <div class="card" style="width:200px;">
                        <div class="card-header m-auto">
                            <img src="{{ asset('storage/product/' . $p->foto) }}" alt="product image"
                                style="width: 100%; height: 130px; object-fit: cover; padding: 0;">
                        </div>
                        <a href="{{ route('product.detail', $p->id) }}" class="text-decoration-none">
                            <div class="card-body text-dark">
                                <p class="m-0 text-justify"> {{ $p->nama_product }} </p>
                            </div>
                        </a>
                        <div class="ms-3">
                            <p class="m-0 text-justify"><i class="fa-regular fa-star"></i> 5+</p>
                        </div>
                        <div class="card-footer d-flex flex-row justify-content-between align-items-center">
                            <p class="m-0" style="font-size: 14px; font-weight: 600;">
                                <span>IDR</span>{{ number_format($p->harga) }}
                            </p>
                            <form action="{{ route('addTocart') }}" method="POST">
                                @csrf
                                <input type="hidden" name="idProduct" value="{{ $p->id }}">
                                <button type="submit" class="btn btn-outline-primary" style="font-size:24px">
                                    <i class="fa-solid fa-cart-plus"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.kategory').change(function(e) {
                e.preventDefault();
                var kategories = [];
                $('input[name="kategory"]:checked').each(function() {
                    kategories.push($(this).val());
                });

                $.ajax({
                    type: "GET",
                    url: "{{ route('shop') }}",
                    data: {
                        kategory: kategories.join(','),
                    },
                    success: function(response) {
                        var products = response.products.data;
                        var html = '';

                        if (products.length > 0) {
                            products.forEach(function(product) {
                                html += `
                                        <div class="card" style="width: 200px;">
                                            <div class="card-header m-auto">
                                                <img src="{{ asset('storage/product/') }}/${product.foto}" alt="product image" style="width: 100%; height: 130px; object-fit: cover; padding: 0;">
                                            </div>
                                            <div class="card-body">
                                                <p class="m-0 text-justify" style="font-size: 14px;">${product.nama_product}</p>
                                                <p class="m-0"><i class="fa-regular fa-star"></i> 5+</p>
                                            </div>
                                            <div class="card-footer d-flex flex-row justify-content-between align-items-center">
                                                <p class="m-0" style="font-size: 14px; font-weight: 600;"><span>IDR</span>${product.harga.toLocaleString()}</p>
                                                <button class="btn btn-outline-primary" style="font-size: 24px">
                                                    <i class="fa-solid fa-cart-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                `;
                            });
                        } else {
                            html = '<h1>Belum ada produk ...!</h1>';
                        }

                        $('#filterResult').html(html);
                    }
                });
            });
        });
    </script>
@endsection
