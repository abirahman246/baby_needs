@extends('pelanggan.layout.index')

@section('content')
    <h4 class="text-center mt-md-5 pt-5 mb-md-2 fw-bold">Contact Us</h4>
    <hr class="mb-5">
    <div class="row  mb-md-5">
        <div class="col-md-5">
            <div class="bg-transparent mb-5" style="width: 100%; height:50vh; border-radius:10px;">
                <img src="{{ asset('assets/images/logo.png') }}" style="width:100%" alt="">
            </div>
        </div>
        <div class="col-md-7 mt-5">
            <div class="card">
                <div class="card-header text-center">
                    <h4>Kritik dan saran</h4>
                </div>
                <div class="card-body">
                    <p class="p-0 mb-5 text-lg-center">
                        Masukan kritik dan saran anda kepada kami agar kami
                        dapat memberikan apa yang menjadi kebutuhan anda dan kami dapat berkembang lebih baik lagi.
                    </p>
                    <div class="mb-3 row">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email" value=""
                                placeholder="Masukan email Anda">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="pesan" class="col-sm-2 col-form-label">Pesan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="pesan" placeholder="Masukan Pesan Anda">
                        </div>
                    </div>
                    <button class="btn btn-primary mt-4 w-100"> Kirim </button>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-lg-between mt-5">
        <div class="d-flex align-items-center gap-4">
            <i class="fa fa-users fa-2x"></i>
            <p class="m-0 fs-5">+300 Pelanggan</p>
        </div>
        <div class="d-flex align-items-center gap-4">
            <i class="fa-solid fa-user-tie fa-2x"></i>
            <p class="m-0 fs-5"> +500 Seller</p>
        </div>
        <div class="d-flex align-items-center gap-4">
            <i class="fa-solid fa-baby-carriage fa-2x"></i>
            <p class="m-0 fs-5">+300 Product</p>
        </div>
    </div>
@endsection
