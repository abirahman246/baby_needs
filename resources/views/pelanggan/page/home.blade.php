@extends('pelanggan.layout.index')

<!-- Modal Detail Produk -->
<div class="modal fade" id="detailProdukModal" tabindex="-1" aria-labelledby="detailProdukModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailProdukModalLabel">Detail Produk</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="detailProdukContent"></div>
            </div>
        </div>
    </div>
</div>

@section('content')

    <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active"
                aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active" data-bs-interval="10000">
                <img src="{{ asset('assets/images/slide1.jpg') }}" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item" data-bs-interval="2000">
                <img src="{{ asset('assets/images/slide2.jpg') }}" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{ asset('assets/images/slide3.jpg') }}" class="d-block w-100" alt="...">
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <h4 class="mt-5">Best Seller</h4>
    <div class="content mt-3 d-flex flex-lg-wrap mb-5 bg-light">
        @if ($best->count() == 0)
            <div class="container"></div>
        @else
            @foreach ($best as $b)
                <div class="card my-3 mx-3" style="width:220px;">
                    <div class="card-header m-auto" style="height:100%;width:100%;">
                        <img src="{{ asset('storage/product/' . $b->foto) }}" alt="baju 1"
                            style="width: 100%;height:200px; object-fit: cover; padding:0;">
                    </div>
                    <a href="{{ route('product.detail', $b->id) }}" class="text-decoration-none">
                        <div class="card-body text-dark">
                            <p class="m-0 text-justify"> {{ $b->nama_product }} </p>
                        </div>
                    </a>
                    <div class="ms-3">
                        <p class="m-0 text-justify"><i class="fa-regular fa-star"></i> 5+</p>
                    </div>
                    <div class="card-footer d-flex flex-row justify-content-between align-items-center">
                        <p class="m-0" style="font-size: 16px; font-weight:600;"><span>IDR
                            </span>{{ number_format($b->harga) }}</p>
                        <form action="{{ route('addTocart') }}" method="POST">
                            @csrf
                            <input type="hidden" name="idProduct" value="{{ $b->id }}">
                            <button type="submit" class="btn btn-outline-primary" style="font-size:24px">
                                <i class="fa-solid fa-cart-plus"></i>
                            </button>
                        </form>
                    </div>
                </div>
            @endforeach
        @endif
    </div>

    <h4 class="mt-5">List Product</h4>
    <div class="content mt-3 d-flex flex-lg-wrap mb-5 bg-light">
        @if ($data->isEmpty())
            <h1>Belum ada product ...!</h1>
        @else
            @foreach ($data as $p)
                <div class="card my-3 mx-3" style="width:220px;">
                    <div class="card-header m-auto" style="height:100%;width:100%;">
                        <img src="{{ asset('storage/product/' . $p->foto) }}" alt="baju 1"
                            style="width: 100%;height:200px; object-fit: cover; padding:0;">
                    </div>
                    <a href="{{ route('product.detail', $p->id) }}" class="text-decoration-none">
                        <div class="card-body text-dark">
                            <p class="m-0 text-justify"> {{ $p->nama_product }} </p>
                        </div>
                    </a>
                    <div class="ms-3">
                        <p class="m-0 text-justify"><i class="fa-regular fa-star"></i> 5+</p>
                    </div>
                    <div class="card-footer d-flex flex-row justify-content-between align-items-center">
                        <p class="m-0" style="font-size: 16px; font-weight:600;"><span>IDR
                            </span>{{ number_format($p->harga) }}</p>
                        <form action="{{ route('addTocart') }}" method="POST">
                            @csrf
                            <input type="hidden" name="idProduct" value="{{ $p->id }}">
                            <button type="submit" class="btn btn-outline-primary" style="font-size:24px">
                                <i class="fa-solid fa-cart-plus"></i>
                            </button>
                        </form>
                    </div>
                </div>
            @endforeach
        @endif
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#detailProdukModal').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                var idProduk = button.data('idproduk');
                var modal = $(this);

                $.ajax({
                    url: '/product/detail/' + idProduk,
                    method: 'GET',
                    success: function(response) {
                        modal.find('#detailProdukContent').html(response);
                    },
                    error: function(xhr, status, error) {
                        console.error(error);
                    }
                });
            });
        });
    </script>
@endsection
