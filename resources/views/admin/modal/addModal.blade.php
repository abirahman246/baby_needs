<div class="modal fade" id="addModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-md">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel">{{ $title }}</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('addData') }}" enctype="multipart/form-data" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="mb-3 row">
                        <label for="SKU" class="col-sm-5 col-form-label">SKU</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control-plaintext" id="SKU" name="sku"
                                value="{{$sku}}" readonly>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="nameProduct" class="col-sm-5 col-form-label">Nama Product</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="nameProduct" name="nama">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="type" class="col-sm-5 col-form-label">Type Product</label>
                        <div class="col-sm-7">
                            <select type="text" class="form-control" id="type" name="type">
                                <option value=""> Pilih Type </option>
                                <option value="Pakaian Bayi">Pakaian Bayi</option>
                                <option value="Perlengkapan Mandi">Perlengkapan Mandi</option>
                                <option value="Peralatan Makan & Minum">Peralatan Makan & Minum</option>
                                <option value="Peralatan Kesehatan">Peralatan Kesehatan</option>
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="kategori" class="col-sm-5 col-form-label">Kategori Product</label>
                        <div class="col-sm-7">
                            <select type="text" class="form-control" id="kategori" name="kategori">
                                <option value=""> Pilih Kategori </option>
                                <option value="Baju Tidur">Baju Tidur</option>
                                <option value="Bodysuit">Bodysuit</option>
                                <option value="Anak-Topi & Kaus Kaki">Topi & Kaus Kaki</option>
                                <option value="Jaket & Selimut">Jaket & Selimut</option>
                                <option value="Sabun">Sabun</option>
                                <option value="Shampoo">Shampoo</option>
                                <option value="Handuk">Handuk</option>
                                <option value="Bak Mandi">Bak Mandi</option>
                                <option value="Botol Susu">Botol Susu</option>
                                <option value="Dot">Dot</option>
                                <option value="Sendok & Garpu">Sendok & Garpu</option>
                                <option value="Piring & Mangkuk">Piring & Mangkuk</option>
                                <option value="Termometer">Termometer</option>
                                <option value="Krim Ruam Popok">Krim Ruam Popok</option>
                                <option value="Aspirator Hidung">Aspirator Hidung</option>
                                <option value="Obat-obatan">Obat-obatan</option>
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="harga" class="col-sm-5 col-form-label">Harga Product</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="harga" name="harga">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="quantity" class="col-sm-5 col-form-label">Qty Product</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="quantity" name="quantity">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="foto" class="col-sm-5 col-form-label">Foto Product</label>
                        <div class="col-sm-7">
                            <input type="hidden" name="foto">
                            <img class="mb-2 preview"
                                style="width: 100px;">
                            <input type="file" class="form-control" accept=".png, .jpg, .jpeg" id="inputFoto"
                                name="foto" onchange="previewImg()">
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function previewImg() {
        const fotoIn = document.querySelector('#inputFoto');
        const preview = document.querySelector('.preview');

        preview.style.display = 'block';

        const oFReader = new FileReader();
        oFReader.readAsDataURL(fotoIn.files[0]);

        oFReader.onload = function(oFREvent) {
            preview.src = oFREvent.target.result;
        }
    }
</script>