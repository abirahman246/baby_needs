<div class="modal fade" id="editModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-md">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel">{{ $title }}</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('updateData', $data->id) }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="modal-body">
                    <div class="mb-3 row">
                        <label for="SKU" class="col-sm-5 col-form-label">SKU</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control-plaintext" id="SKU" name="sku"
                                value="{{ $data->sku }}" readonly>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="nameProduct" class="col-sm-5 col-form-label">Nama Product</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="nameProduct" name="nama"
                                value="{{ $data->nama_product }}">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="type" class="col-sm-5 col-form-label">Type Product</label>
                        <div class="col-sm-7">
                            <select type="text" class="form-control" id="type" name="type">
                                <option value=""> Pilih Type </option>
                                <option value="Pakaian Bayi" {{ $data->type === 'pakaian Bayi' ? 'selected' : '' }}>
                                    Pakaian Bayi
                                </option>
                                <option value="Perlengkapan Mandi"
                                    {{ $data->type === 'Perlengkapan Mandi' ? 'selected' : '' }}>
                                    Perlengkapan Mandi</option>
                                <option value="Peralatan Makan & Minum"
                                    {{ $data->type === 'Peralatan Makan & Minum' ? 'selected' : '' }}>Peralatan Makan &
                                    Minum
                                </option>
                                <option value="Peralatan Kesehatan"
                                    {{ $data->type === 'Peralatan Kesehatan' ? 'selected' : '' }}>Peralatan Kesehatan
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="kategori" class="col-sm-5 col-form-label">Kategori Product</label>
                        <div class="col-sm-7">
                            <select type="text" class="form-control" id="kategori" name="kategori">
                                <option value=""> Pilih Kategori </option>
                                <option value="Baju Tidur" {{ $data->kategory === 'Baju Tidur' ? 'selected' : '' }}>
                                    Baju Tidur</option>
                                <option value="Bodysuit"
                                    {{ $data->kategory === 'Bodysuit' ? 'selected' : '' }}>Bodysuit
                                </option>
                                <option value="Topi & Kaus Kaki"
                                    {{ $data->kategory === 'Topi & Kaus Kaki' ? 'selected' : '' }}>
                                    Topi & Kaus Kaki</option>
                                <option value="Jaket & Selimut"
                                    {{ $data->kategory === 'Jaket & Selimut' ? 'selected' : '' }}>
                                    Jaket & Selimut</option>
                                    <option value="Sabun" {{ $data->kategory === 'Sabun' ? 'selected' : '' }}>
                                    Sabun</option>
                                    <option value="Shampoo" {{ $data->kategory === 'Shampoo' ? 'selected' : '' }}>
                                    Shampoo</option>
                                    <option value="Handuk" {{ $data->kategory === 'Handuk' ? 'selected' : '' }}>
                                    Handuk</option>
                                    <option value="Bak Mandi" {{ $data->kategory === 'Bak Mandi' ? 'selected' : '' }}>
                                    Bak Mandi</option>
                                    <option value="Botol Susu" {{ $data->kategory === 'Botol Susu' ? 'selected' : '' }}>
                                    Botol Susu</option>
                                    <option value="Dot" {{ $data->kategory === 'Dot' ? 'selected' : '' }}>
                                    Dot</option>
                                    <option value="Sendok & Garpu" {{ $data->kategory === 'Sendok & Garpu' ? 'selected' : '' }}>
                                    Sendok & Garpu</option>
                                    <option value="Piring & Mangkuk" {{ $data->kategory === 'Piring & Mangkuk' ? 'selected' : '' }}>
                                    Piring & Mangkuk</option>
                                    <option value="Termometer" {{ $data->kategory === 'Termometer' ? 'selected' : '' }}>
                                    Termometer</option>
                                    <option value="Krim Ruam Popok" {{ $data->kategory === 'Krim Ruam Popok' ? 'selected' : '' }}>
                                    Krim Ruam Popok</option>
                                    <option value="Aspirator Hidung" {{ $data->kategory === 'Aspirator Hidung' ? 'selected' : '' }}>
                                    Aspirator Hidung</option>
                                    <option value="Obat-obatan" {{ $data->kategory === 'Obat-obatan' ? 'selected' : '' }}>
                                    Obat-obatan</option>
                            </select>

                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="harga" class="col-sm-5 col-form-label">Harga Product</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="harga" name="harga"
                                value="{{ $data->harga }}">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="quantity" class="col-sm-5 col-form-label">Qty Product</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="quantity" name="quantity"
                                value="{{ $data->quantity }}">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="foto" class="col-sm-5 col-form-label">Foto Product</label>
                        <div class="col-sm-7">
                            <input type="hidden" name="foto" value="{{ $data->foto }}">
                            <img src="{{ asset('storage/product/' . $data->foto) }}" class="mb-2 preview"
                                style="width: 100px;">
                            <input type="file" class="form-control" accept=".png, .jpg, .jpeg" id="inputFoto"
                                name="foto" onchange="previewImg()">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function previewImg() {
        const fotoIn = document.querySelector('#inputFoto');
        const preview = document.querySelector('.preview');

        preview.style.display = 'block';

        const oFReader = new FileReader();
        oFReader.readAsDataURL(fotoIn.files[0]);

        oFReader.onload = function(oFREvent) {
            preview.src = oFREvent.target.result;
        }
    }
</script>
